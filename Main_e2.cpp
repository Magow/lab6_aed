#include<iostream>
#include <cstdlib>
#include<ctime>
// include clases necesita ir con ""
#include "clases_alg.h"
 

using namespace std; 

int main()
{
    srand(time(0));
    //ejemplo.ordenarMerg();

    
    int opcion;
    bool funciona = true;
    bool condicion1 = true;

    int tamanno = 0;

    cout << "Porfavor no ingresar strings o cosas raras al valor :( \n";
    cout << "Si desea molestar puede ingresar valores negativos ;) \n";

    while (condicion1)
    {   
        // Ingreso de tamaño de arreglo 
        cout << "Ingrese el tamaño del arreglo a utilizar: ";
        // definir tamaño de arreglo 
        cin >> tamanno;
        
        if(tamanno <= 0)
        {
            cout << "El valor ingresado debe ser de tipo entero\n";
        }
        else
        {
            cout << "El tamaño del arreglo ha sido ingresado de manera correcta, ahora puede ejecutar el programa";
            condicion1 = false;
        }
    }
    // declaración fuera del bucle 
    ArrayData data_arr(tamanno);
    

    do {
    
        // Texto del menú que se verá cada vez
        cout << "\n\nMenu de Opciones" << endl;
        cout << "1. Opcion 1: Mostrar arreglo original (sin ordenar)" << endl;
        cout << "2. Opcion 2: Mostrar ejecución de algoritmo burbuja y arreglo resultante" << endl;
        cout << "3. Opcion 3: Mostrar arreglo ya ordenado por algoritmo de bubblesort" << endl;
        cout << "4. Opcion 4: Mostrar ejecución de algoritmo mergesort y arreglo resultante" << endl;
        cout << "5. Opcion 5: Mostrar arreglo ya ordenado por algoritmo de mergesort" << endl;
        cout << "0. SALIR" << endl;

        cout << "\nIngrese una opcion: ";
        cin >> opcion;
        
        switch (opcion) {

            case 1:
                // Mostrar el primer arreglo original sin alteraciones
                cout << "Mostrando arreglo original: ";
                data_arr.mostrarElementosOriginal();

                break;
                
            case 2:
                // Mostrar arreglo ordenado por algoritmo de bubblesort con su tiempo de ejecución
                if(data_arr.estadoBubblesort == true){
                    cout << "Se muestra el proceso de ordenamiento del arreglo por algoritmo bubblesort"<< endl;
                    data_arr.ordenarBubb();
                }
                else
                {
                    cout << "El algoritmo ya fue ejecutado, imprima sus datos con la opción 3"<< endl;
                }
                  
                break;
                
            case 3:
                // Mostrar datos del arreglo final ordenado por bubblesort
                
                if(data_arr.estadoBubblesort == false)
                {
                    cout << "Se muestra arreglo ordenado por algoritmo bubblesort" << endl;
                    data_arr.mostrarElementosBubble();
                    cout << "Se muestra el tiempo de ejecución del programa" << endl;
                    data_arr.mostrarTiempoBubble();
                }
                else 
                {   
                    cout << "Ejecute el algoritmo antes de pedir su resultado" << endl;
                }
                
                /*
                cout << "Se muestra arreglo ordenado por algoritmo bubblesort:  ";
                data_arr.mostrarElementosBubble();
                */
                break;
            
            case 4:
                // Mostrar arreglo ordenado por algoritmo de mergesort con su tiempo de ejecución
                if(data_arr.estadoMergesort == true){
                    cout << "\nSe muestra el proceso de ordenamiento del arreglo por algoritmo mergesort"<< endl;
                    data_arr.ordenarMerg();
                    break;
                }
                else
                {
                    cout << "El algoritmo ya fue ejecutado, imprima sus datos con la opción 5"<< endl;
                }

                break;
            case 5:
                // Mostrar datos del arreglo final ordenado por mergesort
                
                if(data_arr.estadoMergesort == false){
                    cout << "Se muestra arreglo ordenado por algoritmo mergesort" << endl;
                    data_arr.mostrarElementosMerge();
                    cout << "\nSe muestra el tiempo de ejecución del programa" << endl;
                    data_arr.mostrarTiempoMerge();
                }
                else 
                {   
                    cout << "Ejecute el algoritmo antes de pedir su resultado" << endl;
                }
                break;
            
            case 0:
            	funciona = false;
            	break;
        }
    } while (funciona);
    
    return 0;
}