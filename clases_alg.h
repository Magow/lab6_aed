#include<vector>

using namespace std;

class ArrayData{
    public:
        bool estadoBubblesort;
        bool estadoMergesort;
        ArrayData(int);

        void ordenarBubb();
        void ordenarMerg();
        void mostrarElementosOriginal();
        void mostrarElementosBubble();
        void mostrarElementosMerge();
        void mostrarTiempoBubble();
        void mostrarTiempoMerge();

    private:
        int tamanno;
        float timeBubble;
        float timeMerge;
        vector<int> dataOriginal;
        vector<int> dataBubblesort;
        vector<int> dataMergesort;
        // idea para bloquear acceso a función de mostrar arreglos


        void ordenarArrayDataBubb(int, int);
        void ordenarArrayDataMerg(int, int);
        void mezclar(int, int, int, int);
        // mostrar arreglos 
        void mostrarSubArreglo(int, int, int);
        //void mostrarSubArregloOriginal(int, int, int);
        void mostrarTiempo(int);
};