#include<iostream>
#include<vector>
#include<cstdlib>
#include<ctime>

#include "clases_alg.h"

using namespace std;



clock_t inicioBubble_t, finBubble_t, inicioMerge_t, finMerge_t;
double totalBubble_t, total_Merge_t;



ArrayData::ArrayData(int tamannoArreglo){
    // si el arreglo es menor a 0 elementos se establece un arreglo forzado de 10 elementos para ejecutar el programa
    // si el tamannoArreglo es mayor a 0 se mantiene su valor, si es menor se establece como 10
    tamanno = (tamannoArreglo > 0) ? tamannoArreglo: 10;
    
    // agregar datos al vector "datos", ahí se almacenan los datos para usar en el algoritmo ArrayData
    for(int i=0; i< tamanno; i++)
    {
    //valores desde el 10 hasta el 99 
        dataOriginal.push_back(10 + rand() % 90);
    }

        for(int i=0; i< tamanno; i++)
    {
        dataBubblesort.push_back(dataOriginal[i]);   
        dataMergesort.push_back(dataOriginal[i]); 
    }
    estadoBubblesort = true;
    estadoMergesort = true;
}

// llamar a la funcion ordenar
// :: Clase ArrayData llama a ordenar 
void ArrayData::ordenarBubb(){
    ordenarArrayDataBubb(0, tamanno-1);
    inicioBubble_t = clock();
    estadoBubblesort = false;
}
void ArrayData::ordenarArrayDataBubb(int inferior, int superior){
    int i, j;
    
    for(i = 0; i <= superior-1; i++)
    {
        for(j = i + 1; j <= superior; j++)
        {
            int temporal;
            if (dataBubblesort[i] > dataBubblesort [j])
            {   
                cout << "Ordenando " << " " << dataBubblesort[i];
                cout << " por " << " " << dataBubblesort[j] << endl;

                temporal = dataBubblesort[i];
                dataBubblesort[i] = dataBubblesort[j];
                dataBubblesort[j] = temporal;
            }
        }
    }
    mostrarSubArreglo(inferior, superior, 2);
    finBubble_t = clock();
    totalBubble_t = (double)(finBubble_t - inicioBubble_t)/CLOCKS_PER_SEC;
    timeBubble = totalBubble_t;
}
void ArrayData::ordenarMerg(){
    ordenarArrayDataMerg(0, tamanno-1);
    inicioMerge_t = clock();    
    estadoMergesort = false;
}

void ArrayData::ordenarArrayDataMerg(int inferior, int superior){
    //Se puede "particionar" el arreglo???
    if (inferior < superior){
        int mitad = (inferior + superior) / 2;

        cout << "\nDividiendo!\n";
        mostrarSubArreglo(inferior, mitad, 3);
        cout << "\n        ";
        mostrarSubArreglo(mitad+1, superior, 3);
        cout << "\n\n";

        ordenarArrayDataMerg(inferior, mitad);
        ordenarArrayDataMerg(mitad+1, superior);

        mezclar(inferior, mitad, mitad+1, superior);
    }

}

void ArrayData::mezclar(int inicio1, int fin1, int inicio2, int fin2){
    int indice1 = inicio1;
    int indice2 = inicio2;

    int indiceNuevo = inicio1;
    vector<int> combinacionArreglo(tamanno);
    
    //////Se muestran los dos subarreglos a mezclar
    cout << "A mezclar:  ";
    mostrarSubArreglo(inicio1, fin1, 3);
    cout << "\n";
    mostrarSubArreglo(inicio2, fin2, 3);
    cout << "\n";


    while (indice1 <= fin1 && indice2 <= fin2){
        if (dataMergesort[indice1] <= dataMergesort[indice2])
            combinacionArreglo[indiceNuevo++] = dataMergesort[indice1++];
        else
            combinacionArreglo[indiceNuevo++] = dataMergesort[indice2++];
    }

    if (indice1 == inicio2){
        while (indice2 <= fin2)
            combinacionArreglo[indiceNuevo++] = dataMergesort[indice2++];
    }else{
        while (indice1 <= fin1)
            combinacionArreglo[indiceNuevo++] = dataMergesort[indice1++];
    } 

    //Copiar Arreglo de Combinacion en Arreglo de Datos original
    for(int i=inicio1; i <= fin2; i++)
    {
        dataMergesort[i] = combinacionArreglo[i];
    }
        
    finMerge_t = clock();
    total_Merge_t = (double)(finMerge_t - inicioMerge_t)/CLOCKS_PER_SEC;
    timeMerge = total_Merge_t;

    cout << "      ";
    mostrarSubArreglo(inicio1, fin2, 3);
    cout << "\n\n";

}

void ArrayData::mostrarElementosOriginal(){
    mostrarSubArreglo(0, tamanno-1, 1);
}
void ArrayData::mostrarElementosBubble(){
    mostrarSubArreglo(0, tamanno-1, 2);
}
void ArrayData::mostrarElementosMerge(){
    mostrarSubArreglo(0, tamanno-1, 3);
}
void ArrayData::mostrarSubArreglo(int inferior, int superior, int arr){
    //for(int i=0; i< inferior; i++)
      //  cout << " ";
    
    // ciclo for para mostrar los datos desde el primero o inferior
    // hasta el último o superior (tamanno - 1)
    if(arr == 1)
    {
        for(int i=inferior; i<=superior; i++)
            cout << " " << dataOriginal[i];
    }

    if(arr == 2)
    {   
        for(int i=inferior; i<=superior; i++)
            cout << " " << dataBubblesort[i];
    }

    if(arr == 3)
    {
        for(int i=inferior; i<=superior; i++)
            cout << " " << dataMergesort[i];
    }
}

void ArrayData::mostrarTiempoBubble(){
    mostrarTiempo(2);
}
void ArrayData::mostrarTiempoMerge(){
    mostrarTiempo(3);
}
void ArrayData::mostrarTiempo(int opcion){
    if(opcion == 2)
    {
        cout << "\nTiempo de ejecución del algoritmo de burbuja = " << " " << timeBubble;
    }
    if(opcion == 3)
    {
        cout << "\nTiempo de ejecución del algoritmo de mezcla = " << " " << timeMerge;
    }
}