#include<iostream>

using namespace std;

int main()
{
    // valor ingresado para buscar en el arreglo
    int valor_buscar;
    cout << "numero a nuscar";
    cin >> valor_buscar;

    //valores necesarios para el programa
    int primero, mitad, ultimo, posicion;               //15
    int vector[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,22,28,33,34,35,44,55,66,77,89,90,91};
    // valores auxiliares para ejecucuón 
    bool x;
    x = true;

    primero = 0;
    ultimo = (sizeof(vector)/sizeof(vector[0]));


    cout << primero << " " << endl;
    cout << ultimo << " " << endl;

    // busqueda binaria 
    while(primero <= ultimo && x == true)
    {
        mitad = (primero + ultimo)/2;
        if(valor_buscar == vector[mitad])
        {   
            posicion = vector[mitad];
            x = false;
        } 
        if(valor_buscar < vector[mitad])
        {
            ultimo = mitad - 1;
        }
        if(valor_buscar > vector[mitad])
        {
            primero = mitad + 1;
        }

    }
    if(x == false)
    {
        cout << "El numero se encuentra en el vector" << endl;
        cout << "Posición:" << " " << posicion;
    }
    else 
    {
        cout << "no está";
    }

    return 0;
}